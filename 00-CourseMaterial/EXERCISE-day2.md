# Angular Workshop



## Exercise 5: Forms

Start with the project: `40-ToDoApp/04-Simple-ToDo-components-solution`.

In the ToDo Application implement a proper `form` for the entry of a new todo.

Duplicate the component and implement it once with a template driven form and a second time with a reactive form.  
Add a validation that the text should be at least 3 characters.  
As a reference study the examples in `10-demos/src/app/02-forms/`.

*Optional:* Find out how to implement a custom validation: The text should begin with a capital letter. Try to implement that rule for the reactive and for the template driven approach.



## Exercise 6: Backend Access

The directory `40-ToDoApp/_server` contains a simple API-Server implementing basic CRUD functionality for our ToDo application.
Start the server with the following commands:

```
npm install #just once
npm start
```

You should now get an array with two todo items at the url: `http://localhost:3456/todos`.

Your task is now to access this backend API from the ToDo application:

- When the application is loaded, all the todo items should be loaded from the server
- When a todo item is added, it should be saved to the server
- When a todo item  is completed it should be updated on the server
- When a todo item  is deleted, it should be deleted from the server.

Start from `40-ToDoApp/11-Simple-ToDo-backend-exercise`.
This project already loads the  todo items from the server when the application is loaded.

The API implemented by the REST-Endpoint is described in the table below:

| HTTP-Method | URL (example)                                                | Request-Body                            | Response                    |
| ----------- | ------------------------------------------------------------ | --------------------------------------- | --------------------------- |
| GET         | http://localhost:3456/todos   *(optional query-parameter: ?completed=0 or 1)* |                                         | {data: [{*todo*},{*todo*}]} |
| GET         | http://localhost:3456/todos/1                                |                                         | {data: {*todo*} }           |
| POST        | http://localhost:3456/todos                                  | { "title": "Test", "completed": false}  | {data: {*todo*} }           |
| PUT         | http://localhost:3456/todos/1                                | { "title": "Test 2", "completed": true} | *empty*                     |
| DELETE      | http://localhost:3456/todos/1                                |                                         | *empty*                     |

Note that all responses are wrapped in a response object with a `data` property.
This is a typical security measure of JSON endpoints. See: http://stackoverflow.com/questions/3503102/what-are-top-level-json-arrays-and-why-are-they-a-security-risk

**Hint:**  
Have a look at `10-basic-constructs/src/app/03-BackendAccess/02-backend-crud` to see an implementation of how to access the todo item API endpoint.