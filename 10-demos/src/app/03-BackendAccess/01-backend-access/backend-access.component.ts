import { Component, OnInit } from '@angular/core';
import { Commit, FirstService } from './first.service';

@Component({
  selector: 'aw-backend-access',
  template: `
    <h4>Latest Angular Committers:</h4>

    <ul>
      <li *ngFor="let commit of commits">{{ commit.date | date }}: {{ commit.name }}</li>
    </ul>

    <p>Error Message: {{ errorMessage }}</p>
  `
})
export class BackendAccessComponent implements OnInit {
  commits: Commit[] = [];
  errorMessage = '';

  constructor(private firstService: FirstService) {}

  ngOnInit(): void {
    this.firstService
      .getData() // use .requestData() instead to get access to the full response object
      .subscribe(
        commits => (this.commits = commits),
        error => (this.errorMessage = error)
      );
  }
}

// TODO:
// Use async pipe: Skip the subscribe and expose the observable as property
//
// <ul>
//   <li *ngFor="#commit of commits | async">
//   {{commit.date | date}}: {{commit.name}}
// </li>
// </ul>
