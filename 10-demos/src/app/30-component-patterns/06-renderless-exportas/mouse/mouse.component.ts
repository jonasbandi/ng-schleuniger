import { Component } from '@angular/core';

type MouseState = {
  x: number;
  y: number;
};

@Component({
  selector: 'aw-mouse',
  exportAs: 'mouse',
  template: `
    <div (mousemove)="handleMouseMove($event)">
      <ng-content></ng-content>
    </div>
  `
})
export class MouseComponent {
  private theState: MouseState = { x: 0, y: 0 };
  get state(): MouseState {
    return this.theState;
  }

  handleMouseMove(event: MouseEvent): void {
    this.theState = {
      x: event.clientX,
      y: event.clientY
    };
  }
}
