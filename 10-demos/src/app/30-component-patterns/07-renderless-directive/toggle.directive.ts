import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

type Toggle = {
  on: boolean;
  setOn: () => {};
  setOff: () => {};
  toggle: () => {};
};

@Directive({ selector: '[awToggle]' })
export class ToggleDirective implements OnInit {
  on = true;
  @Input('awToggleOn') initialState = true; // tslint:disable-line:no-input-rename

  constructor(private tpl: TemplateRef<{ $implicit: Toggle }>, private vcr: ViewContainerRef) {}

  ngOnInit(): void {
    this.on = this.initialState;
    this.vcr.createEmbeddedView(this.tpl, {
      $implicit: { on: this.on, setOn: this.setOn, setOff: this.setOff, toggle: this.toggle }
    });
  }

  setOn(): void {
    this.on = true;
  }
  setOff(): void {
    this.on = false;
  }
  toggle(): void {
    this.on = !this.on;
  }
}
