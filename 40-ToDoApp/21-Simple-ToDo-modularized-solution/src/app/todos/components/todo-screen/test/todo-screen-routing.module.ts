import { Routes, RouterModule } from '@angular/router';

import { TodoScreenMainComponent } from './todo-screen-main.component';

const appRoutes: Routes = [{ path: '', component: TodoScreenMainComponent }];

export const ToDoScreenRoutingModule = RouterModule.forChild(appRoutes);
