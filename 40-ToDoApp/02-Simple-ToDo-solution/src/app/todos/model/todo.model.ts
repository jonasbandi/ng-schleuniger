export class ToDo {
  private _title = ''; //tslint:disable-line
  completed = false;

  constructor(title: string = '') {
    this.title = title.trim();
  }

  get title(): string {
    return this._title;
  }
  set title(value: string) {
    this._title = value.trim();
  }
}
