import { Component, EventEmitter, Output } from '@angular/core';
import { ToDo } from '../../model/todo.model';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'td-new-todo',
  templateUrl: './new-todo.component.html',
})
export class NewTodoComponent {
  theForm: FormGroup;
  @Output() addToDo = new EventEmitter<ToDo>();

  constructor(formBuilder: FormBuilder) {
    this.theForm = formBuilder.group({
      newToDoTitle: [''],
      // newToDoTitle: ['', [Validators.required, Validators.minLength(3), validateTodo]]
    });

    // programmatic changes of validation
    setTimeout(() => this.enableValidation(), 5000);
  }

  onSubmit(): void {
    console.log(this.theForm);
    this.addToDo.emit(new ToDo(this.theForm.value.newToDoTitle));
    // this.theForm.setValue({'newToDoTitle': ''});
    this.theForm.reset();
  }

  enableValidation(): void {
    console.log('changing validators...');
    const titleControl = this.theForm.get('newToDoTitle');
    titleControl?.setValidators([Validators.required, Validators.minLength(3), validateTodo]);
    titleControl?.updateValueAndValidity();
  }
}

const validateTodo: ValidatorFn = (control) => {
  const firstChar = control.value && control.value.charAt(0);

  if (!firstChar || firstChar === firstChar.toUpperCase()) {
    return null;
  } else {
    return { capitalLetter: { valid: false } };
  }
};
